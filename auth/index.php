<?php
    session_start();
    if (isset($_SESSION['login']) && !isset($_POST['login'])) { //  MENGECEK JIKA USER SUDAH LOGIN
        $url = "./";
        if ($_SESSION['login_as'] == 'admin') {
            $url = "../?page=buat_jadwal";
        } else if ($_SESSION['login_as'] == 'atasan') {
            $url = "../?page=lihat_jadwal_laporan";
        } else {
            $url = "../?page=lihat_jadwal_saya";
        }
        header("Location: ".$url);
        die();
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login </title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100" style="background-image: url('images/bg-02.jpg');">
			<div class="wrap-login100">
				<form action="" method="post" class="login100-form validate-form">

					<?php
						if (isset($_POST['login'])) {	// MENGECEK JIKA BUTTON LOGIN DITEKAN
							if ($_POST['username'] == 'admin' && $_POST['password'] == 'admin') {	// JIKA USERNAME DAN PASSWORD = ADMIN
								$_SESSION['login'] = true;	// SET LOGIN = TRUE AGAR TAHU USER SUDAH LOGIN
								$_SESSION['login_as'] = 'admin';	// SET LOGIN SEBAGAI ADMIN
								header("Location: ../?page=buat_jadwal");
								die();
							} else if ($_POST['username'] == 'atasan' && $_POST['password'] == 'atasan') {	// JIKA USERNAME DAN PASSWORD = ADMIN
								$_SESSION['login'] = true;	// SET LOGIN = TRUE AGAR TAHU USER SUDAH LOGIN
								$_SESSION['login_as'] = 'atasan';	// SET LOGIN SEBAGAI ADMIN
								header("Location: ../?page=lihat_jadwal_laporan");
								die();
							} else {
								// MEMASUKAN FILE KONEKSI, DATABASE DAN M_PEGAWAI DARI FOLDER MODELS
								require_once('../config/koneksi.php');
								require_once('../models/database.php');
								require_once('../models/m_pegawai.php');
							
								$connection = new Database($host, $user, $pass, $database);	// CONNECT KE DTABASE
								$pgw = new Pegawai($connection);

								$login = $pgw->tampil($_POST['username']);	// MENGAMBIL DATA DARI TBL_PEGAWAI BERDASARKAN USERNAME/NIP
								if ($login && @$login->num_rows > 0) {	// MENGECEK JIKA USER DITEMUKAN
									$login = $login->fetch_object();	// MENGAMBIL DATA
									if ($login->PASSWORD == $_POST['password']) {	// JIKA PASSWORD SAMA/BENAR
										$_SESSION['login'] = true;
										$_SESSION['login_as'] = strtolower($login->JABATAN);
										$_SESSION['login_user'] = $login;
										if ($_SESSION['login_as'] == 'atasan') {
											$url = "../?page=lihat_jadwal_laporan";
										} else {
											$url = "../?page=lihat_jadwal_saya";
										}
										header("Location: ".$url);
										die();
									} else {
										echo '<div style="color: red;width:100%;text-align: center;">Password Salah</div>';
									}
								} else {
									echo '<div style="color: red;width:100%;text-align: center;">User Tidak Ditemukan</div>';
								}
							}
						}
					?>

					<span class="login100-form-title p-b-34 p-t-27">
						Log in
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Enter NIP">
						<input class="input100" type="text" name="username" placeholder="NIP">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<input class="input100" type="password" name="password" placeholder="Password">
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
					</div>
					<input type="hidden" name="login" value="login"/>
					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Login
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>