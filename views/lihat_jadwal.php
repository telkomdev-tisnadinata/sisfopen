<?php
    include "models/m_jadwal.php";
    $jwl = new Jadwal($connection);
?>
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <!-- column -->
    <div class="col-lg-12">
        <div class="card">
            <div class="card-block">
                <h4 class="card-title">Daftar Tugas</h4>
                <?php
                    if (isset($_GET['hapus']) || isset($_GET['selesai'])) {
                        if (isset($_GET['hapus'])) {
                            $data = $jwl->hapus($_GET['hapus']);
                            $message = 'Dihapus';
                        }
                        if (isset($_GET['selesai'])) {
                            $sql = "UPDATE tbl_jadwal j, tbl_penugasan p SET j.STATUS = 'FINISHED', j.TANGGAL_SELESAI = NOW(), p.TANGGAL_SELESAI = NOW() WHERE j.KODE_JADWAL = '".$_GET['selesai']."' AND j.KODE_JADWAL = p.KODE_JADWAL";
                            $data = $jwl->query($sql);
                            $message = 'Diubah Menjadi Selesai';
                        }
                        if ($data) {
                            echo '
                            <div class="alert alert-success"> <i class="ti-user"></i> Data Berhasil '.$message.'.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                            </div>
                            ';
                        } else {
                            echo '
                            <div class="alert alert-danger"> <i class="ti-user"></i> Data Gagal '.$message.'.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                            </div>
                            ';
                        }
                    }
                ?>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>NO</th>
                                <th>Nama Acara</th>
                                <th>Produser Acara</th>
                                <th>Jumlah Karyawan</th>
                                <th>Tanggal Mulai</th>
                                <th>Tanggal Selesai</th>
                                <th>Jam Shift</th>
                                <th> </th>
                        
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no = 1;
                        $tampil = $jwl->tampil_jadwal_by_status("STATUS != 'DELETED'");
                        if (!$tampil) {
                        ?>
                            <tr>
                                <td colspan="7">Tidak Dapat Menampilkan Data</td>
                            </tr>
                        <?php
                        } else {
                            while($data = $tampil->fetch_object()){
                                $jumlah_karyawan = $jwl->query('SELECT NIP FROM tbl_penugasan WHERE KODE_JADWAL = '.$data->KODE_JADWAL);
                                $jumlah_karyawan = $jumlah_karyawan->num_rows;
                            ?>
                            <tr>
                                <td><?php echo $no++; ?></td>
                                <td><?php echo $data->NAMA_ACARA; ?></td>
                                <td><?php echo $data->NAMA.' ('.$data->PRODUSER_NIP.')'; ?></td>
                                <td><?php echo $jumlah_karyawan; ?> orang</td>
                                <td><?php echo $data->TANGGAL_MULAI; ?></td>
                                <td><?php echo $data->TANGGAL_SELESAI; ?></td>
                                <td><?php echo $data->JAM; ?></td>
                                <td>
                                    
                                    <a href="?page=lihat_jadwal_detail<?php echo '&detail='.$data->KODE_JADWAL;?>"> <button type="button" class="btn waves-effect waves-light btn-info btn-xs">DETAIL</button></a>
                                    <a href="?page=lihat_jadwal<?php echo '&hapus='.$data->KODE_JADWAL;?>" onclick="return konfirmasiHapus('<?php echo $data->NAMA_ACARA; ?>');"> <button type="button" class="btn waves-effect waves-light btn-danger btn-xs">HAPUS</button></a>
                                    <?php
                                        if ($data->STATUS != 'FINISHED') {
                                            ?>
                                    <a href="?page=ubah_jadwal<?php echo '&ubah='.$data->KODE_JADWAL;?>"> <button type="button" class="btn waves-effect waves-light btn-warning btn-xs">UBAH</button></a>
                                    
                                    <a href="?page=lihat_jadwal<?php echo '&selesai='.$data->KODE_JADWAL;?>"> <button type="button" class="btn waves-effect waves-light btn-success btn-xs">TANDAI SELESAI</button></a>
                                            <?php
                                        } else {
                                            ?>
                                    <a href="#"> <button type="button" class="btn waves-effect waves-light btn-success btn-xs">ACARA SELESAI</button></a>
                                            <?php
                                        }
                                    ?>
                                </td>
                            </tr>
                            <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->