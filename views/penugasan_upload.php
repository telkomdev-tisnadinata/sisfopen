<?php
    include "models/m_jadwal.php";
    include "models/m_penugasan.php";
    include "models/m_pegawai.php";
    $tgs = new Penugasan($connection);
    $pgw = new Pegawai($connection);
    $jwl = new Jadwal($connection);
?>

<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <!-- column -->
    <div class="col-lg-12">
        <div class="card">
            <div class="card-block">
                <?php
                    if (isset($_POST['upload'])) {
                        $data = $tgs->upload_file($_POST, $_FILES);
                        $message = 'Diupload';
                        if ($data === 'too large') {
                            echo '
                            <div class="alert alert-danger"> <i class="ti-user"></i> Ukuran file terlalu besar, maksimal 200 megabyte.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                            </div>
                            ';
                        } else if ($data) {
                            echo '
                            <div class="alert alert-success"> <i class="ti-user"></i> Data Berhasil '.$message.'.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                            </div>
                            ';
                        } else {
                            echo '
                            <div class="alert alert-danger"> <i class="ti-user"></i> Data Gagal '.$message.'.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                            </div>
                            ';
                        }
                    }
                    $NIP = $_SESSION['login_user']->NIP;
                    $sql = "SELECT * FROM tbl_penugasan p, tbl_jadwal j WHERE p.NIP = '".$NIP."' AND p.KODE_JADWAL = j.KODE_JADWAL AND j.STATUS != 'DELETED' ORDER BY j.CREATED_AT DESC";
                    $tampil = $tgs->query($sql);
                    $sql = "SELECT * FROM tbl_jadwal j, tbl_pegawai p WHERE j.PRODUSER_NIP = '".$NIP."' AND j.PRODUSER_NIP = p.NIP AND j.STATUS != 'DELETED'";
                    $tampil_produser = $tgs->query($sql);
            ?>
                <form action="" class="form-material" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nip" class="col-md-12">PILIH JADWAL ACARA</label>
                                <div class="col-md-12">
                                    <select class="custom-select col-12" id="ID_PENUGASAN" name="ID_PENUGASAN">
                                        <?php
                                            while ($data = $tampil_produser->fetch_object()) {
                                                echo '<option value="PRODUSER-'.$data->KODE_JADWAL.'">'.$data->NAMA_ACARA.', Produser : '.$data->NAMA.' ('.$data->PRODUSER_NIP.')'.'</option>';
                                            }
                                            while ($data = $tampil->fetch_object()) {
                                                $get_produser = $pgw->tampil_filter('NIP', $data->PRODUSER_NIP)->fetch_object();
                                                echo '<option value="PEGAWAI-'.$data->ID_PENUGASAN.'">'.$data->NAMA_ACARA.', Produser : '.$get_produser->NAMA.' ('.$data->PRODUSER_NIP.')'.'</option>';
                                            }
                                        ?>                                        
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="nama" class="col-md-12">Pilih File <small style="color: red;">(MAKSIMAL SIZE 200MB)</small></label>
                                <div class="col-md-12">
                                    <input type="file" class="form-control" name="FILE" id="FILE" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                            <label for="nama" class="col-md-12">&nbsp;</label>
                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-success" name="upload" value="UNGGAH"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-block">
                <h4 class="card-title">Daftar Upload Penugasan</h4>
                <div class="table-responsive">
                    <table class="table" style="overflow-y:scroll;max-height:750px;display:block;">
                        <thead>
                            <tr><th>NO</th>
                                <th>Nama Acara</th>
                                <th>Produser Acara</th>
                                <th>Sebagai</th>
                                <th>Lokasi</th>
                                <th>Bukti Upload</th>                        
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no = 1;
                        $sql = "SELECT p.*, j.NAMA_ACARA, j.PRODUSER_NIP, j.LOKASI, j.JAM, j.STATUS FROM tbl_penugasan p, tbl_jadwal j WHERE p.NIP = '".$NIP."' AND p.KODE_JADWAL = j.KODE_JADWAL AND j.STATUS != 'DELETED' ORDER BY j.CREATED_AT DESC";
                        $tampil = $tgs->query($sql);
                        $sql = "SELECT * FROM tbl_jadwal j, tbl_pegawai p WHERE j.PRODUSER_NIP = '".$NIP."' AND j.PRODUSER_NIP = p.NIP AND j.STATUS != 'DELETED'";
                        $tampil_produser = $tgs->query($sql);
                        if (!$tampil && !$tampil_produser) {
                        ?>
                            <tr>
                                <td colspan="7">Tidak Dapat Menampilkan Data</td>
                            </tr>
                        <?php
                        } else {
                            while ($data = $tampil_produser->fetch_object()) {
                            ?>
                            <tr>
                                <td><?php echo $no++; ?></td>
                                <td><?php echo $data->NAMA_ACARA; ?></td>
                                <td><?php echo $data->NAMA.' ('.$data->PRODUSER_NIP.')'; ?></td>
                                <td><?php echo $_SESSION['login_user']->JABATAN; ?></td>
                                <td><?php echo $data->LOKASI; ?></td>
                                <td>
                                    <a target="_blank" href="<?php echo $data->BUKTI_TUGAS; ?>"><?php echo @explode('/',@$data->BUKTI_TUGAS)[2]; ?></a>
                                </td>
                            </tr>
                            <?php
                            }
                            while($data = $tampil->fetch_object()){
                                $get_produser = $pgw->tampil_filter('NIP', $data->PRODUSER_NIP)->fetch_object();
                            ?>
                            <tr>
                                <td><?php echo $no++; ?></td>
                                <td><?php echo $data->NAMA_ACARA; ?></td>
                                <td><?php echo $get_produser->NAMA.' ('.$data->PRODUSER_NIP.')'; ?></td>
                                <td><?php echo $_SESSION['login_user']->JABATAN; ?></td>
                                <td><?php echo $data->LOKASI; ?></td>
                                <td>
                                    <a target="_blank" href="<?php echo $data->BUKTI_TUGAS; ?>"><?php echo @explode('/',@$data->BUKTI_TUGAS)[2]; ?></a>
                                </td>
                            </tr>
                            <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->