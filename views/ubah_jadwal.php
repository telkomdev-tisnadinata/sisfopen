<?php
    include "models/m_pegawai.php";
    include "models/m_jadwal.php";
    include "models/m_penugasan.php";
    $pgw = new Pegawai($connection);
    $jwl = new Jadwal($connection);
    $tgs = new Penugasan($connection);
?>
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <!-- column -->
    <div class="col-lg-12">
        <div class="card">
            <div class="card-block">
            <h4 class="card-title">Form Data Acara</h4>
            <hr>
                <?php
                    if (isset($_POST['ubah'])) {
                        $_POST['KODE_JADWAL'] = $_GET['ubah'];
                        $_POST['LOKASI'] = $_POST['LOKASI'].' ('.$_POST['LOKASI2'].')';
                        $data = $jwl->ubah($_POST);
                        $message = 'Diubah';
                        if ($data) {
                            echo '
                            <div class="alert alert-success"> <i class="ti-user"></i> Data Berhasil '.$message.'.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                            </div>
                            ';
                        } else {
                            echo '
                            <div class="alert alert-danger"> <i class="ti-user"></i> Data Gagal '.$message.'.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                            </div>
                            ';
                        }
                    }
                    $NAMA_ACARA = '';
                    $PRODUSER_NIP = '';
                    $TANGGAL_MULAI = '';
                    $TANGGAL_SELESAI = '';
                    $LOKASI = '';
                    $JAM = '';
                    if (isset($_GET['ubah'])) {
                        $acara = $jwl->tampil($_GET['ubah']);
                        $acara = $acara->fetch_object();
                        $NAMA_ACARA = $acara->NAMA_ACARA;
                        $PRODUSER_NIP = $acara->PRODUSER_NIP;
                        $TANGGAL_MULAI = $acara->TANGGAL_MULAI;
                        $TANGGAL_SELESAI = $acara->TANGGAL_SELESAI;
                        $DATA_LOKASI = explode(' (',$acara->LOKASI);
                        $LOKASI = $DATA_LOKASI[0];
                        $DATA_LOKASI = explode(')',$DATA_LOKASI[1]);
                        $LOKASI2 = $DATA_LOKASI[0];
                        $JAM = $acara->JAM;

                        $petugas = $tgs->tampil_active($_GET['ubah']);
                        $list_petugas = [];
                        for($p = 0; $p < count($petugas) ; $p++) {
                            $p_temp = $petugas[$p];
                            array_push($list_petugas, $p_temp->NIP);
                        }
                    }
                ?>
                <form acion="" method="post" class="form-material">
                    <?php
                    echo '
                        <input type="hidden" id="KODE_JADWAL" value="'.$_GET['ubah'].'"/>
                    ';
                    for($i=0 ; $i < count($list_petugas) ; $i++){
                            echo '
                                <input type="hidden" name="LIST_PEGAWAI['.$i.']" value="'.$list_petugas[$i].'"/>
                            ';
                        }
                    ?>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nama-acara" class="col-md-12">NAMA ACARA</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" name="NAMA_ACARA" id="NAMA_ACARA" value="<?php echo $NAMA_ACARA; ?>" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for ="nama-produser"class="col-md-12">Pilih Produser Acara</label>
                                <div class="col-md-12">
                                    <select class="custom-select col-12" id="PRODUSER_NIP" name="PRODUSER_NIP">
                                        <?php
                                            $produser = $pgw->tampil_filter('JABATAN', 'PRODUSER');
                                            if (!$produser || @$produser->num_rows == 0) {
                                                echo '<option>Tidak Ada Pegawai Dengan Jabatan Produser</option>';
                                            } else {
                                                while ($data = $produser->fetch_object()) {
                                                    $selected = "";
                                                    if ($PRODUSER_NIP == $data->NIP) {
                                                        $selected = "selected";
                                                    }
                                                    echo '<option value="'.$data->NIP.'" '.$selected.'>'.$data->NAMA.' - '.$data->NIP.'</option>';
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="TANGGAL_MULAI_OLD" id="TANGGAL_MULAI_OLD" value="<?php echo $TANGGAL_MULAI; ?>"/>
                        <input type="hidden" name="TANGGAL_SELESAI_OLD" id="TANGGAL_SELESAI_OLD" value="<?php echo $TANGGAL_SELESAI; ?>"/>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="tanggal-mulai" class="col-md-12">Tanggal Mulai</label>
                                <div class="col-md-12">
                                    <input type="date" class="form-control form-control-line"name="TANGGAL_MULAI" id="TANGGAL_MULAI" value="<?php echo $TANGGAL_MULAI; ?>" onChange="getListPegawaiUbah('mulai');" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="tanggal-mulai" class="col-md-12">Tanggal Selesai</label>
                                <div class="col-md-12">
                                    <input type="date" class="form-control form-control-line"name="TANGGAL_SELESAI" id="TANGGAL_SELESAI" value="<?php echo $TANGGAL_SELESAI; ?>" onChange="getListPegawaiUbah('selesai');" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for ="nama-produser"class="col-md-12">Pilih Jam Acara</label>
                                <div class="col-md-12">
                                    <select class="custom-select col-12" id="JAM" name="JAM">
                                        <option value="Tidak dalam shift" <?php echo ($JAM == "Tidak Dalam shift") ? "selected" : ''; ?>>Tidak Dalam Shift</option>
                                        <option value="Shift 1 ( Pukul 08:00 - 12:00 )" <?php echo ($JAM == "Shift 1 ( Pukul 08:00 - 12:00 )") ? "selected" : ''; ?>>Shift 1 ( Pukul 08:00 - 12:00 )</option>
                                        <option value="Shift 2 ( Pukul 13:00 - 17:00 )" <?php echo ($JAM == "Shift 2 ( Pukul 13:00 - 17:00 )") ? "selected" : ''; ?>>Shift 2 ( Pukul 13:00 - 17:00 )</option>
                                        <option value="Shift 3 ( Pukul 18:00 - 21:00 )" <?php echo ($JAM == "Shift 3 ( Pukul 18:00 - 21:00 )") ? "selected" : ''; ?>>Shift 3 ( Pukul 18:00 - 21:00 )</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="tanggal-mulai" class="col-md-12">Lokasi Acara</label>
                                <div class="col-md-12">
                                    <select class="custom-select col-12" id="LOKASI" name="LOKASI">
                                        <option value="DIDALAM KANTOR" <?php echo ($LOKASI == "DIDALAM KANTOR") ? "selected" : ''; ?>>DIDALAM KANTOR</option>
                                        <option value="DILUAR KANTOR" <?php echo ($LOKASI == "DILUAR KANTOR") ? "selected" : ''; ?>>DILUAR KANTOR</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="tanggal-mulai" class="col-md-12">Keterangan Lokasi</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" name="LOKASI2" id="LOKASI2" placeholder="nama studio, dsb" value="<?php echo $LOKASI2; ?>" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <br>
                            <h4 class="card-title">Form Penugasan Pegawai</h4>
                            <hr>
                        </div>
                        <div class="col-md-12" id="form_pilih_pegawai">
                            <div class="form-group">
                                <small>
                                    <ul>
                                        <li>Pilih pegawai yang akan ditugaskan</li>
                                        <li>Pegawai yang tidak dapat dipilih, berarti sudah ditugaskan pada jadwal lain</li>
                                    </ul>
                                </small>
                                <input type="hidden" name="JUMLAH_PEGAWAI" value="0"/>
                                <?php
                                    $jabatan = $pgw->query('SELECT JABATAN FROM tbl_pegawai WHERE JABATAN != "PRODUSER" GROUP BY JABATAN ORDER BY JABATAN ASC');
                                    if (!$jabatan || @$jabatan->num_rows == 0) {
                                        echo '<option>Tidak Ada Pegawai Selain Produser</option>';
                                    } else {
                                        $no=0;
                                        while ($row = $jabatan->fetch_object()) {
                                            $pegawai = $pgw->query('SELECT NIP, NAMA FROM tbl_pegawai WHERE JABATAN = "'.$row->JABATAN.'" ORDER BY NAMA ASC');
                                            if (!$pegawai || @$pegawai->num_rows == 0) {
                                                echo '<option>Tidak Ada</option>';
                                            } else {
                                                ?>
                                                    <label class="col-12 col-form-label"><strong><?php echo strtoupper($row->JABATAN);?></strong></label>
                                                    <div class="input-group">
                                                        <div class="col-md-12">
                                                <?php
                                                while ($data = $pegawai->fetch_object()) {
                                                    $status = "";
                                                    if (isset($_GET['ubah'])) {
                                                        if (in_array($data->NIP, $list_petugas)) {
                                                            $status = "checked";
                                                        } else{
                                                            $TANGGAL = array(
                                                                'MULAI' => date("Y-m-d"),
                                                                'SELESAI' => date("Y-m-d")
                                                            );
                                                            $checkDuty = $pgw->status_penugasan($data->NIP, $TANGGAL);
                                                            $status = ($checkDuty->num_rows != 0) ? 'disabled' : '';
                                                        }
                                                    }
                                                    ?>
                                                        <div class="col-md-4" style="float: left;">
                                                            <div class="input-group-text">
                                                                <input type="checkbox" name="pegawai[<?php echo $no;?>]" id="pegawai[<?php echo $no;?>]" value="<?php echo $data->NIP;?>" class="filled-in chk-col-cyan" <?php echo $status;?>>
                                                                <label for="pegawai[<?php echo $no;?>]" class="mb-0"><?php echo ucfirst($data->NAMA);?></label>
                                                            </div>
                                                        </div>
                                                    <?php
                                                    $no++;
                                                }
                                                ?>
                                                        <input type="hidden" name="TOTAL_PEGAWAI" value="<?php echo $no;?>"/>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                <?php
                                            }        
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="submit" class="btn btn-warning" name="ubah" value="UBAH & SIMPAN JADWAL"/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>        
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
